import Vue from 'vue'
import { createRenderer } from 'vue-server-renderer'
import express from 'express'

const server = express()
const renderer = createRenderer()


const app = new Vue({template: "<div>hi there!!</div>"})

server.get('*', async (req, res) => {
    
    try {
        const html = await renderer.renderToString(app)

        res.end(`
            <!DOCTYPE html>
            <html lang="en">
                <head><title>Hello</title></head>
                <body>${html}</body>
            </html>
            `)
    } catch (ex) {
        res.status(500).end('Internal Server Error')
        return
    }
    
})

server.listen(8080)



